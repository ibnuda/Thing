package me.ijauradunbi.thing.domain.interactor.base

/**
 * Created on 05/09/2016.
 */
interface Interactor {
    operator fun invoke(): Event
}