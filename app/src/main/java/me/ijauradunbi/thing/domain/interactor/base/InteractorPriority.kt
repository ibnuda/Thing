package me.ijauradunbi.thing.domain.interactor.base

/**
 * Created on 05/09/2016.
 */

enum class InteractorPriority(val value: Int) {
    LOW(0),
    MID(500),
    HIGH(1000)
}
