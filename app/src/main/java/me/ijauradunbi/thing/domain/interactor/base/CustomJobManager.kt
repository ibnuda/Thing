package me.ijauradunbi.thing.domain.interactor.base

import android.content.Context
import com.path.android.jobqueue.JobManager
import com.path.android.jobqueue.config.Configuration

/**
 * Created on 05/09/2016.
 */

class CustomJobManager(context: Context) : JobManager(context, CustomJobManager.getJobManagerConfiguration(context)) {
    companion object {
        private fun getJobManagerConfiguration(context: Context): Configuration {
            return Configuration.Builder(context)
                    .minConsumerCount(1)
                    .maxConsumerCount(3)
                    .loadFactor(3)
                    .consumerKeepAlive(120)
                    .build()
        }
    }
}
