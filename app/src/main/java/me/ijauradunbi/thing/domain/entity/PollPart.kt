package me.ijauradunbi.thing.domain.entity

/**
 * Created on 05/09/2016.
 */

data class PollPart(
        val by: String,
        val id: Int,
        val parent: Int,
        val score: Int,
        val text: String,
        val time: Int
)
