package me.ijauradunbi.thing.domain.interactor.event

import me.ijauradunbi.thing.domain.entity.Story
import me.ijauradunbi.thing.domain.interactor.base.Event

/**
 * Created on 06/09/2016.
 */

data class StoryEvent(val story: Story?): Event
