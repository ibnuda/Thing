package me.ijauradunbi.thing.domain.interactor.base

/**
 * Created on 05/09/2016.
 */
interface Bus {
    fun post(event: Any): Unit
    fun register(observer: Any): Unit
    fun unregister(observer: Any): Unit
}