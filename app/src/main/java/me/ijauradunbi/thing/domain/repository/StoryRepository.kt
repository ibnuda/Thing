package me.ijauradunbi.thing.domain.repository

import me.ijauradunbi.thing.domain.entity.Story

/**
 * Created on 05/09/2016.
 */

interface StoryRepository {
    fun getTopStory(): List<Story>
    fun getStory(id: Int): Story?
}
