package me.ijauradunbi.thing.domain.entity

/**
 * Created on 05/09/2016.
 */

data class Ask(
        val by: String,
        val descendants: Int,
        val id: Int,
        val kids: List<Int>,
        val score: Int,
        val text: String,
        val time: Int,
        val title: String
)
