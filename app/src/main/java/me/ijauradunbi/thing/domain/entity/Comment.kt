package me.ijauradunbi.thing.domain.entity

/**
 * Created on 05/09/2016.
 */

data class Comment(
        val by: String,
        val id: Int,
        val kids: List<Int>,
        val parent: Int,
        val text: String,
        val time: Int
)
