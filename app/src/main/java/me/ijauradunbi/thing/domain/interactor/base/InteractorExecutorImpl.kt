package me.ijauradunbi.thing.domain.interactor.base

import com.path.android.jobqueue.JobManager

/**
 * Created on 05/09/2016.
 */

class InteractorExecutorImpl(val jobManager: JobManager, val bus: Bus) : InteractorExecutor {
    override fun execute(interactor: Interactor, priority: InteractorPriority) {
        jobManager.addJob(InteractorWrapper(interactor, priority, bus))
    }
}
