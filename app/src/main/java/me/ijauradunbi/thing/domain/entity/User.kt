package me.ijauradunbi.thing.domain.entity

/**
 * Created on 05/09/2016.
 */

data class User(
        val about: String,
        val created: Int,
        val delay: Int,
        val id: String,
        val karma: Int,
        val submitted: List<Int>
)
