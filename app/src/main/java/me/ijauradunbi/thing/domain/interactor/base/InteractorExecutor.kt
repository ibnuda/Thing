package me.ijauradunbi.thing.domain.interactor.base

/**
 * Created on 05/09/2016.
 */

interface InteractorExecutor {
    fun execute(interactor: Interactor, priority: InteractorPriority = InteractorPriority.LOW)
}
