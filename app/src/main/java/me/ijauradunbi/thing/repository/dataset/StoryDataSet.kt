package me.ijauradunbi.thing.repository.dataset

import me.ijauradunbi.thing.domain.entity.Story

/**
 * Created on 06/09/2016.
 */

interface StoryDataSet {
    fun requestTopStories(): List<Story>
    fun requestStory(id: Int): Story?
}
