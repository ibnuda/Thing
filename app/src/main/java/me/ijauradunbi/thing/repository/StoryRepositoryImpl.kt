package me.ijauradunbi.thing.repository

import me.ijauradunbi.thing.domain.entity.Story
import me.ijauradunbi.thing.domain.repository.StoryRepository
import me.ijauradunbi.thing.repository.dataset.StoryDataSet

/**
 * Created on 06/09/2016.
 */

class StoryRepositoryImpl(val storyDataSets: List<StoryDataSet>) : StoryRepository {
    override fun getTopStory(): List<Story> {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getStory(id: Int): Story {
        throw UnsupportedOperationException("not implemented")
    }
}
