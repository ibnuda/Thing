package me.ijauradunbi.thing.data.mapper

/**
 * Created on 06/09/2016.
 */

class StoryMapper {
    fun getStoryUrl(id: Int): String {
        return "https://hacker-news.firebaseio.com/v0/item/$id.json"
    }
}
