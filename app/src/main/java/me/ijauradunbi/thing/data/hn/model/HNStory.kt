package me.ijauradunbi.thing.data.hn.model

import com.google.gson.annotations.SerializedName

/**
 * Created on 06/09/2016.
 */

class HNStory(
        val by: String,
        val descendants: Int,
        val id: Int,
        @SerializedName("kids") val kids: List<Int>,
        val score: Int,
        val time: Int,
        val title: String,
        val url: String
)
