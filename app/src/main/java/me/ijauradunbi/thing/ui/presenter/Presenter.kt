package me.ijauradunbi.thing.ui.presenter

import me.ijauradunbi.thing.domain.interactor.base.Bus

/**
 * Created on 06/09/2016.
 */

interface Presenter<T> {
    val view: T
    val bus: Bus

    fun onResume() {
        bus.register(this)
    }

    fun onPause() {
        bus.unregister(this)
    }
}
