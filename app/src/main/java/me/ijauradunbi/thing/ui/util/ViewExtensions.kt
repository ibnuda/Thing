package me.ijauradunbi.thing.ui.util

import android.animation.ObjectAnimator
import android.support.annotation.LayoutRes
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.view.animation.Interpolator
import me.ijauradunbi.thing.ui.adapter.SingleClickListener
import org.jetbrains.anko.layoutInflater

/**
 * Created on 06/09/2016.
 */

fun View.animateEnter() = animateTranslationY(0, DecelerateInterpolator(3f))
fun View.animateExit() = animateTranslationY(-height, AccelerateInterpolator(3f))

fun View.animateTranslationY(translationY: Int, interpolator: Interpolator) {
    with(ObjectAnimator.ofFloat(this, "translationY", translationY.toFloat())) {
        duration = context.resources.getInteger(android.R.integer.config_mediumAnimTime).toLong()
        setInterpolator(interpolator)
        start()
    }
}

fun View.singleClick(l: (android.view.View?) -> Unit) {
    setOnClickListener(SingleClickListener(l))
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return context.layoutInflater.inflate(layoutRes, this, attachToRoot)
}
