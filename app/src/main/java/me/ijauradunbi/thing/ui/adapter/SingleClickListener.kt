package me.ijauradunbi.thing.ui.adapter

import android.view.View
import android.view.ViewConfiguration

/**
 * Created on 06/09/2016.
 */

class SingleClickListener(val click: (view: View) -> Unit) : View.OnClickListener {

    companion object {
        private val DOUBLE_CLICK_TIMEOUT = ViewConfiguration.getDoubleTapTimeout()
    }

    private var lastClick = 0L

    override fun onClick(view: View) {
        if (getLastClickTimeout() > DOUBLE_CLICK_TIMEOUT) {
            lastClick = System.currentTimeMillis()
            click(view)
        }
    }

    private fun getLastClickTimeout() : Long {
        return System.currentTimeMillis() - lastClick
    }
}
