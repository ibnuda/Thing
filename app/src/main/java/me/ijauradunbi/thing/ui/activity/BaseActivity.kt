package me.ijauradunbi.thing.ui.activity

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import me.ijauradunbi.thing.R
import org.jetbrains.anko.find

/**
 * Created on 05/09/2016.
 */

abstract class BaseActivity : AppCompatActivity() {
    protected abstract val layoutResource: Int
    // TODO : create a proper layout.
    val toolbar: Toolbar by lazy { find<Toolbar>(R.id.action_bar_title) }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        setContentView(layoutResource)
        setSupportActionBar(toolbar)
    }
}
